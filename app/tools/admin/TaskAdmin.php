<?php

/**
 * 任务设置
 * @author  Mr.L <349865361@qq.com>
 */

namespace app\tools\admin;

class TaskAdmin extends \app\system\admin\SystemExtendAdmin {

    protected $_model = 'toolsTask';

    /**
     * 模块信息
     */
    protected function _infoModule() {
        return [
            'info' => [
                'name' => '任务管理',
                'description' => '管理系统中执行任务',
            ],
            'fun' => [
                'index' => true,
                'del' => true,
            ]
        ];
    }

    public function _indexParam() {
        return [
            'keyword' => 'title'
        ];
    }

}