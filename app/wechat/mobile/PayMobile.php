<?php

/**
 * 微信支付
 */

namespace app\wechat\mobile;

class PayMobile extends  \app\member\mobile\MemberMobile {

    protected $_middle = 'wechat/MobilePay';

    public function index() {
        $data = request('get');
        target($this->_middle, 'middle')->meta()->export(function () use ($data) {
            $this->assign('data', $data);
            $this->memberDisplay();
        }, function ($message, $code, $url) {
            $this->errorCallback($message, $code, $url);
        });
    }

}