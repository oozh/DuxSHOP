<?php

/**
 * 银行管理
 * @author  Mr.L <349865361@qq.com>
 */

namespace app\member\admin;

class PayCardAdmin extends \app\system\admin\SystemExtendAdmin {

    protected $_model = 'PayCard';

    /**
     * 模块信息
     */
    protected function _infoModule() {
        return [
            'info' => [
                'name' => '银行卡管理',
                'description' => '管理会员银行卡信息',
            ],
            'fun' => [
                'index' => true,
                'add' => true,
                'edit' => true,
                'del' => true,
            ]
        ];
    }

    public function _indexParam() {
        return [
            'keyword' => 'name'
        ];
    }


    public function _addAssign() {
        return [
            'bankList' => target('member/PayBank')->loadList()
        ];
    }

    public function _editAssign() {
        return [
            'bankList' => target('member/PayBank')->loadList()
        ];
    }

    public function _addBefore() {
        $label = $_POST['label'];
        $info = target('member/PayBank')->getWhereInfo([
            'label' => $label
        ]);
        $_POST['name'] = $info['name'];
    }

    public function _editBefore() {
        $label = $_POST['label'];
        $info = target('member/PayBank')->getWhereInfo([
            'label' => $label
        ]);
        $_POST['name'] = $info['name'];
    }


}