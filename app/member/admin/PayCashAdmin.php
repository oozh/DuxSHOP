<?php

/**
 * 提现管理
 * @author  Mr.L <349865361@qq.com>
 */

namespace app\member\admin;

class PayCashAdmin extends \app\system\admin\SystemExtendAdmin {

    protected $_model = 'PayCash';

    /**
     * 模块信息
     */
    protected function _infoModule() {
        return [
            'info' => [
                'name' => '提现管理',
                'description' => '管理用户提现记录',
            ],
            'fun' => [
                'index' => true
            ]
        ];
    }

    public function _indexParam() {
        return [
            'keyword' => 'C.tel',
            'status' => 'A.status',
            'cash_no' => 'A.cash_no'
        ];
    }

    public function _indexOrder() {
        return 'cash_id desc';
    }

    public function _indexWhere($whereMaps) {
        if ($whereMaps['A.status'] > 2) {
            unset($whereMaps['A.status']);
        }
        return $whereMaps;
    }

    public function info() {
        if (!isPost()) {
            $id = request('get', 'id');
            if (empty($id)) {
                $this->error('参数传递错误!');
            }
            $info = target('member/PayCash')->getInfo($id);
            if (empty($info)) {
                $this->error('暂无该记录!');
            }
            $this->assign('info', $info);
            $this->systemDisplay();
        } else {
            $post = request('post');
            $info = target($this->_model)->getInfo($post['cash_id']);
            if (empty($info)) {
                $this->error('暂无该记录!');
            }
            $data = [
                'cash_id' => $post['cash_id'],
                'status' => $post['status'] ? 2 : 0,
                'complete_time' => time(),
                'auth_admin' => $this->userInfo['user_id'],
                'auth_time' => time(),
                'auth_remark' => $post['remark']
            ];
            target($this->_model)->beginTransaction();

            if (!$post['status']) {
                $status = target('member/Finance', 'service')->account([
                    'user_id' => $info['user_id'],
                    'money' => $info['money'],
                    'pay_no' => target('member/Finance', 'service')->logNo(),
                    'pay_name' => '提现退回',
                    'type' => 1,
                    'deduct' => 1,
                    'remark' => '提现失败退回处理',
                ]);
                if (!$status) {
                    target($this->_model)->rollBack();
                    $this->error(target('member/Finance', 'service')->getError());
                }
            }
            if (!target($this->_model)->edit($data)) {
                target($this->_model)->rollBack();
                $this->error('处理失败,请稍后再试!');
            }
            target($this->_model)->commit();
            $this->success('处理成功!', url('index'));

        }
    }

}