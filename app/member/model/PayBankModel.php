<?php

/**
 * 银行管理
 */
namespace app\member\model;

use app\system\model\SystemModel;

class PayBankModel extends SystemModel {

    protected $infoModel = [
        'pri' => 'bank_id',
        'validate' => [
            'name' => [
                'empty' => ['', '请输入银行名称!', 'must', 'all'],
                'unique' => ['', '已存在相同的银行!', 'value', 'all'],
            ],
            'label' => [
                'empty' => ['', '请输入银行标识!', 'must', 'all'],
                'unique' => ['', '已存在相同的标识!', 'value', 'all'],
            ],
        ],
    ];



}