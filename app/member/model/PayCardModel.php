<?php

/**
 * 银行卡管理
 */
namespace app\member\model;

use app\system\model\SystemModel;

class PayCardModel extends SystemModel {

    protected $infoModel = [
        'pri' => 'card_id',
        'validate' => [
            'name' => [
                'empty' => ['', '请输入银行名称!', 'must', 'all'],
            ],
            'label' => [
                'empty' => ['', '请输入银行标识!', 'must', 'all'],
            ],
            'account' => [
                'card' => ['', '银行卡号码输入错误!', 'must', 'all'],
            ],
            'account_tel' => [
                'empty' => ['', '开户电话输入错误!', 'must', 'all'],
            ],
            'account_name' => [
                'empty' => ['', '请输入开户姓名!', 'must', 'all'],
            ],
            'account_bank' => [
                'empty' => ['', '请输入开户行!', 'must', 'all'],
            ]
        ],
        'format' => [
            'account_name' => [
                'function' => ['html_clear', 'all'],
            ],
            'account_bank' => [
                'function' => ['html_clear', 'all'],
            ],
        ],
    ];

    protected function base($where) {
        return $this->table('pay_card(A)')
            ->join('member_user(B)', ['A.user_id', 'B.user_id'])
            ->field(['A.*', 'B.email(user_email)', 'B.tel(user_tel)', 'B.nickname(user_nickname)'])
            ->where((array)$where);
    }

    public function loadList($where = array(), $limit = 0, $order = '') {
        $list = $this->base($where)
            ->limit($limit)
            ->order($order)
            ->select();
        foreach ($list as $key => $vo) {
            $list[$key]['show_name'] = target('member/MemberUser')->getNickname($vo['user_nickname'], $vo['user_tel'], $vo['user_email']);
        }
        return $list;
    }

    public function countList($where = array()) {
        return $this->base($where)->count();
    }

    public function getWhereInfo($where) {
        $info = $this->base($where)->find();
        if ($info) {
            $info['show_name'] = target('member/MemberUser')->getNickname($info['user_nickname'], $info['user_tel'], $info['user_email']);
        }
        return $info;
    }



}