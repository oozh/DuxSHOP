<?php

/**
 * 会员首页
 */

namespace app\member\mobile;


class IndexMobile extends \app\member\mobile\MemberMobile {

    protected $_middle = 'member/Index';

    public function index() {
        target($this->_middle, 'middle')->setParams([
            'platform' => 'mobile',
            'user_info' => $this->userInfo
        ])->meta()->data()->export(function ($data) {
            $this->assign($data);
            $this->memberDisplay('', false);
        });
    }



}