<?php

/**
 * 头像输出
 */

namespace app\member\middle;


class AvatarMiddle extends \app\base\middle\BaseMiddle {


    protected function avatar() {
        $id = intval($this->params['id']);
        $type = intval($this->params['type']);

        if (empty($id)) {
            $this->stop('头像不存在', 404);
        }
        switch ($type) {
            case 1:
                $type = 32;
                break;
            case 3:
                $type = 128;
                break;
            case 2:
            default:
                $type = 64;
                break;

        }
        $avatar = ROOT_PATH . 'upload/avatar/' . $id . '/'. $type . '.jpg';
        if(!is_file($avatar)) {
            $avatar = ROOT_PATH . 'public/member/images/avatar.jpg';
        }
        return $this->run([
            'file' => $avatar

        ]);
        header('content-type: image/png');
        echo file_get_contents($avatar);
    }

}