<?php

/**
 * 会员充值
 */

namespace app\member\middle;


class RechargeMiddle extends \app\base\middle\BaseMiddle {


    protected function data() {
        $payList = target('member/PayConfig')->typeList(true, $this->params['platform'], false);
        return $this->run([
            'payList' => $payList
        ]);
    }

    protected function recharge() {
        $type = html_clear($this->params['type']);
        $money = intval($this->params['money']);

        if(empty($type)) {
            return $this->stop('请选择支付方式!');
        }
        if($money <= 0) {
            return $this->stop('充值金额错误，只能充值整数!');
        }
        $payList = target('member/PayConfig')->typeList(true, $this->params['platform'], false);
        $payTypeInfo = $payList[$type];
        if (empty($payTypeInfo)) {
            return $this->stop('该支付类型无法使用!');
        }

        $logNo = target('member/Finance', 'service')->logNo($this->params['user_id']);
        $sign = data_sign($logNo);

        $model = target('member/MemberRecharge');
        $model->beginTransaction();
        $data = [];
        $data['user_id'] = $this->params['user_id'];
        $data['money'] = $money;
        $data['recharge_no'] = $logNo;
        $data['status'] = 0;
        $data['create_time'] = time();
        if(!target('member/MemberRecharge')->add($data)) {
            $model->rollBack();
            return $this->stop('充值订单创建失败!');
        }

        $data = target($payTypeInfo['target'], 'pay')->getData([
            'user_id' => $this->params['user_id'],
            'order_no' => $logNo,
            'money' => $money,
            'title' => '会员充值',
            'body' => '会员充值',
            'url' => url('member/Recharge/pay'),
            'app' => 'member',
        ], url('complete', ['pay_no' => $logNo, 'pay_sign' => $sign]));

        if (!$data) {
            $model->rollBack();
            return $this->stop(target($payTypeInfo['target'], 'pay')->getError());
        }
        $model->commit();
        return $this->run($data);
    }

    protected function complete() {
        $payNo = $this->params['pay_no'];
        $paySign = $this->params['pay_sign'];
        if (empty($payNo) || empty($paySign)) {
            return $this->stop('页面不存在', 404);
        }
        if (!data_sign_has($payNo, $paySign)) {
            return $this->stop('页面不存在', 404);
        }

        $name = '充值成功';
        $desc = '充值完成,请等待系统处理!';

        return $this->run([
            'status' => 1,
            'name' => $name,
            'desc' => $desc
        ]);
    }

    protected function log() {
        $type = intval($this->params['type']);
        $userId = intval($this->params['user_id']);
        $this->params['limit'] = intval($this->params['limit']);
        $where = [];
        $where['A.user_id'] = $userId;
        $pageLimit = $this->params['limit'] ? $this->params['limit'] : 20;
        $model = target('member/MemberRecharge');
        $count = $model->countList($where);
        $pageData = $this->pageData($count, $pageLimit);
        $list = $model->loadList($where, $pageData['limit'], 'recharge_id desc');

        return $this->run([
            'type' => $type,
            'pageData' => $pageData,
            'countList' => $count,
            'pageList' => $list,
            'pageLimit' => $pageLimit
        ]);
    }

}