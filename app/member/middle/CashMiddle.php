<?php

/**
 * 账户提现
 */

namespace app\member\middle;


class CashMiddle extends \app\base\middle\BaseMiddle {

    private $_model = 'member/PayCash';


    protected function meta($title = '', $name = '', $url = '') {
        $this->setMeta($title);
        $this->setName($name);
        $this->setCrumb([
            [
                'name' => '账户提现',
                'url' => url('member/Cash/index')
            ],
            [
                'name' => $name,
                'url' => $url
            ]
        ]);

        return $this->run([
            'pageInfo' => $this->pageInfo
        ]);
    }


    protected function data() {
        $type = intval($this->params['type']);
        $userId = intval($this->params['user_id']);
        $this->params['limit'] = intval($this->params['limit']);
        if ($type == 1) {
            $where['A.status'] = 1;
        }
        if ($type == 2) {
            $where['A.status'] = 2;
        }
        if ($type == 3) {
            $where['A.status'] = 0;
        }
        $where['A.user_id'] = $userId;
        $pageLimit = $this->params['limit'] ? $this->params['limit'] : 20;

        $model = target($this->_model);
        $count = $model->countList($where);
        $pageData = $this->pageData($count, $pageLimit);
        $list = $model->loadList($where, $pageData['limit'], 'create_time desc');

        return $this->run([
            'type' => $type,
            'pageData' => $pageData,
            'countList' => $count,
            'pageList' => $list,
            'pageLimit' => $pageLimit
        ]);
    }

    protected function info() {
        $cashNo = html_clear($this->params['no']);
        $info = target('member/PayCash')->getWhereInfo([
            'A.cash_no' => $cashNo,
            'A.user_id' => intval($this->params['user_id'])
        ]);
        if (empty($info)) {
            return $this->stop('该提现单不存在!');
        }
        return $this->run([
            'info' => $info,
        ]);
    }

    private $cardInfo;
    private $userId;
    private function applyTest() {
        $this->userId = intval($this->params['user_id']);
        $this->cardInfo = target('member/PayCard')->getWhereInfo(['A.user_id' => $this->userId]);
        if (empty($this->cardInfo)) {
            return $this->stop('先请添加银行卡', 302, url('member/card/index'));
        }
    }

    protected function apply() {
        $this->applyTest();
        preg_match('/([\d]{4})([\d]{4})([\d]{4})([\d]{4})([\d]{0,})?/', $this->cardInfo['account'], $match);
        unset($match[0]);
        $this->cardInfo['account'] = implode(' ', $match);
        return $this->run([
            'cardInfo' => $this->cardInfo
        ]);
    }

    protected function applyPost() {
        $this->applyTest();
        $money = intval($this->params['money']);
        $userInfo = $this->params['user_info'];
        if(empty($money)) {
            return $this->stop('请输入提现金额！');
        }
        if($money > $userInfo['money'] || $money <= 0) {
            return $this->stop('可提现金额不足！');
        }
        $config = target('member/MemberConfig')->getConfig();

        if($money < $config['clear_withdraw']) {
            return $this->stop('最少提现额度为'.$config['clear_withdraw'] . '元');
        }

        if($config['clear_num']) {
            $count = target('member/PayCash')->countList(['_sql' => 'A.status != 0 AND A.create_time >= ' .mktime(0,0,0,date('m'),1,date('Y')) . ' AND A.create_time <= ' . mktime(23,59,59,date('m'),date('t'),date('Y'))]);
            if($count >= $config['clear_num']) {
                return $this->stop('当月提现次已满，请于下月继续提现！');
            }
        }

        $cashNo = target('order/Order', 'service')->logNo();
        $data = [
            'cash_no' => $cashNo,
            'user_id' => $userInfo['user_id'],
            'money' => $money,
            'create_time' => time(),
            'status' => 1,
            'bank' => $this->cardInfo['name'],
            'label' => $this->cardInfo['label'],
            'tax' => $config['clear_tax'],
            'account' => $this->cardInfo['account'],
            'account_tel' => $this->cardInfo['account_tel'],
            'account_name' => $this->cardInfo['account_name'],
            'account_bank' => $this->cardInfo['account_bank'],
        ];
        target('member/PayCash')->beginTransaction();
        if(!target('member/PayCash')->add($data)) {
            target('member/PayCash')->rollBack();
            return $this->stop('提现申请失败!');
        }
        $status = target('member/Finance', 'service')->account([
            'user_id' => $userInfo['user_id'],
            'money' => $money,
            'pay_no' => $cashNo,
            'pay_name' => '账户提现',
            'type' => 0,
            'deduct' => 1
        ]);
        if(!$status) {
            target('member/PayCash')->rollBack();
            return $this->stop(target('member/Finance', 'service')->getError());
        }
        target('member/PayCash')->commit();
        return $this->run([], '提现申请成功！');
    }


}