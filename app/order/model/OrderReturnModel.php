<?php

/**
 * 订单商品退货
 */
namespace app\order\model;

use app\system\model\SystemModel;

class OrderReturnModel extends SystemModel {

    protected $infoModel = [
        'pri' => 'return_id',
    ];

    protected function base($where) {
        $base = $this->table('order_return(A)')
            ->join('member_user(B)', ['B.user_id', 'A.user_id'])
            ->join('order_goods(C)', ['C.id', 'A.order_goods_id']);
        $field = ['A.*', 'B.email(user_email)', 'B.tel(user_tel)', 'B.nickname(user_nickname)', 'B.avatar(user_avatar)', 'C.id(order_goods_id)', 'C.order_id', 'C.goods_name', 'C.goods_name', 'C.goods_image', 'C.goods_url', 'C.goods_qty', 'C.goods_price', 'C.price_total', 'C.extend'];
        return $base->field($field)->where((array)$where);
    }

    public function loadList($where = array(), $limit = 0, $order = '') {
        $list = $this->base($where)
            ->limit($limit)
            ->order('A.return_id desc')
            ->select();
        foreach ($list as $key => $vo) {
            $list[$key]['images'] = unserialize($vo['images']);
            $list[$key]['user_avatar'] = $vo['user_avatar'] ? $vo['user_avatar'] : ROOT_URL . '/public/member/images/avatar.jpg';
            $list[$key]['extend'] = unserialize($vo['extend']);
        }
        return $list;
    }

    public function countList($where = []) {
        return $this->base($where)->count();
    }

    public function getWhereInfo($where) {
        $info = $this->base($where)->find();
        $info['images'] = unserialize($info['images']);
        $info['user_avatar'] = $info['user_avatar'] ? $info['user_avatar'] : ROOT_URL . '/public/member/images/avatar.jpg';
        $info['extend'] = unserialize($info['extend']);
        return $info;
    }

    public function getInfo($id) {
        $where = [];
        $where['A.return_id'] = $id;
        return $this->getWhereInfo($where);
    }

    public function causeList() {
        return [
            '外观/参数等与描述不符',
            '商品发错货',
            '产品质量/故障',
            '效果不好或不喜欢',
            '收到商品少件/破损/污渍',
            '假冒商品',
            '其他',
        ];
    }

}