<?php

/**
 * 订单管理
 */

namespace app\order\model;

use app\system\model\SystemModel;

class OrderModel extends SystemModel {

    protected $infoModel = [
        'pri' => 'order_id',
    ];

    protected function base($where) {
        $base = $this->table('order(A)')
            ->join('member_user(B)', ['B.user_id', 'A.order_user_id']);
        $field = ['A.*', 'B.email(user_email)', 'B.tel(user_tel)', 'B.nickname(user_nickname)'];

        return $base
            ->field($field)
            ->where((array)$where);
    }

    public function loadList($where = [], $limit = 0, $order = 'order_id desc') {
        $list = $this->base($where)
            ->limit($limit)
            ->order($order)
            ->select();
        if (empty($list)) {
            return [];
        }
        foreach ($list as $key => $vo) {
            $list[$key]['show_name'] = target('member/MemberUser')->getNickname($vo['user_nickname'], $vo['user_tel'], $vo['user_email']);
            $list[$key]['total_price'] = price_format($vo['delivery_price'] + $vo['order_price']);
            $list[$key]['status_data'] = target('order/Order', 'service')->getAction($vo);
            $list[$key]['status_data']['html'] = target('order/Order', 'service')->orderActionHtml($list[$key]);
            $list[$key]['order_currency'] = unserialize($vo['order_currency']);

        }
        return $list;
    }

    public function getWhereInfo($where) {
        $info = parent::getWhereInfo($where);
        if (empty($info)) {
            return [];
        }
        $info['show_name'] = target('member/MemberUser')->getNickname($info['user_nickname'], $info['user_tel'], $info['user_email']);
        $info['status_data'] = target('order/Order', 'service')->getAction($info);
        $info['status_data']['html'] = target('order/Order', 'service')->orderActionHtml($info);
        $info['total_price'] = price_format($info['delivery_price'] + $info['order_price']);
        $info['order_currency'] = unserialize($info['order_currency']);

        return $info;
    }

    public function getInfo($id) {
        $where = [];
        $where['order_id'] = $id;

        return $this->getWhereInfo($where);
    }

    public function actionHtml($info) {
    }

    public function getCouponHas($hasId) {
        return [
            'title' => '全品通用优惠券',
            'url' => VIEW_LAYER_NAME . '/order/Cart/index'
        ];
    }

    public function isCoupon($coupon, $order) {
        if($coupon['type'] <> 'common') {
            return false;
        }
        if($coupon['meet_money'] > $order['order_price']) {
            return false;
        }
        return true;
    }

}