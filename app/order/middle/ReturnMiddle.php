<?php

/**
 * 退货管理
 */

namespace app\order\middle;

class ReturnMiddle extends \app\base\middle\BaseMiddle {


    private $_model = 'order/OrderReturn';

    protected function meta($title = '', $name = '', $url = '') {
        $this->setMeta($title);
        $this->setName($name);
        $crumb = [];
        if($url) {
            $crumb = [
                'name' => $name,
                'url' => $url
            ];
        }
        $this->setCrumb([
            [
                'name' => '退货管理',
                'url' => url('index')
            ],
            $crumb
        ]);

        return $this->run([
            'pageInfo' => $this->pageInfo
        ]);
    }

    protected function data() {
        $userId = intval($this->params['user_id']);
        $type = intval($this->params['type']);
        $pageLimit = intval($this->params['limit']);
        $where = [];
        switch($type) {
            case 1:
                $where['A.status'] = 1;
                break;
            case 2:
                $where['A.status'] = 2;
                break;
            case 3:
                $where['A.status'] = 3;
                break;
            case 4:
                $where['A.status'] = 4;
                break;
            case 5:
                $where['A.status'] = 0;
                break;
        }
        $where['A.user_id'] = $userId;
        $pageLimit = $pageLimit ? $pageLimit : 20;

        $model = target($this->_model);
        $count = $model->countList($where);
        $pageData = $this->pageData($count, $pageLimit);
        $list = $model->loadList($where, $pageData['limit'], 'return_id desc');

        return $this->run([
            'type' => $type,
            'pageData' => $pageData,
            'countList' => $count,
            'pageList' => $list,
            'pageLimit' => $pageLimit
        ]);
    }

    protected function info() {
        $returnNo = $this->params['return_no'];
        $userId = intval($this->params['user_id']);
        $info = target($this->_model)->getWhereInfo([
            'return_no' => $returnNo,
            'A.user_id' => $userId
        ]);
        if(empty($info)) {
            return $this->stop('退货单不存在!', 404);
        }
        $orderInfo = target('order/Order')->getInfo($info['order_id']);
        return $this->run([
            'info' => $info,
            'orderInfo' => $orderInfo
        ]);
    }


    protected function orderInfo() {
        $id = intval($this->params['id']);
        $userId = intval($this->params['user_id']);

        $info = target('order/OrderGoods')->getWhereInfo([
            'id' => $id
        ]);

        if (empty($info)) {
            $this->stop('该订单商品不存在');
        }
        if ($info['service_status']) {
            $this->stop('该商品正在售后状态!');
        }

        $target = target('order/Order');
        $orderInfo = $target->getInfo($info['order_id']);

        if ($orderInfo['order_user_id'] <> $userId) {
            return $this->stop('订单不存在！', 404);
        }

        if(!$orderInfo['order_status']) {
            return $this->stop('该订单已关闭!');
        }

        if($orderInfo['status_data']['action'] <> 'parcel' && $orderInfo['status_data']['action'] <> 'delivery' && $orderInfo['status_data']['action'] <> 'receive') {
            return $this->stop('暂时无法进行退款操作!');
        }

        return $this->run([
            'id' => $id,
            'info' => $info,
            'orderInfo' => $orderInfo,
            'causeList' => target($this->_model)->causeList()
        ]);

    }

    protected function delivery() {
        $returnNo = $this->params['return_no'];
        $userId = intval($this->params['user_id']);
        $deliveryName = html_clear($this->params['delivery_name']);
        $deliveryNo = html_clear($this->params['delivery_no']);
        $info = target('order/OrderReturn')->getWhereInfo([
            'return_no' => $returnNo,
            'A.user_id' => $userId
        ]);
        if (empty($info)) {
            return $this->stop('该退货单不存在!');
        }
        if($info['status'] <> 2) {
            return $this->stop('该退货单无法保存!');
        }
        if($info['delivery_name'] || $info['delivery_no'] ) {
            return $this->stop('您已经提交退货快递信息!');
        }
        if(empty($deliveryName) || empty($deliveryNo)) {
            return $this->stop('快递信息未填写完整!');
        }
        $status = target('order/OrderReturn')->edit([
            'return_id' => $info['return_id'],
            'delivery_name' => $deliveryName,
            'delivery_no' => $deliveryNo
        ]);
        if (!$status) {
            target('order/OrderReturn')->rollBack();
            return $this->stop('退货信息保存失败!');
        }
        return $this->run([], '退货信息保存成功!');
    }

    protected function push() {
        $cause = str_len(html_clear($this->params['cause']), 250);
        $content = str_len(html_clear($this->params['content']), 300);
        $money = price_format($this->params['money']);
        $images = $this->params['images'];
        $userId = $this->params['user_id'];

        $info = $this->data['info'];

        if (empty($cause)) {
            return $this->stop('请选择退货原因!');
        }
        if (empty($content)) {
            return $this->stop('请填写退货详情!');
        }
        if(bccomp($money, 0, 2)  !== 1) {
            return $this->stop('请填写退货金额!');
        }
        if(bccomp($money, $info['price_total'], 2) === 1) {
            return $this->stop('退货金额不能大于购买金额!');
        }
        if (!empty($images) && is_array($images)) {
            $httpHost = DOMAIN_HTTP;
            foreach ($images as $image) {
                if (substr($image, 0, 1) <> '/' && strpos($image, $httpHost, 0) === false) {
                    $this->stop('您上传的图片有误,请重新上传!');
                }
            }
        }else {
            $images = [];
        }
        $images = $images ? $images : [];

        target($this->_model)->beginTransaction();
        $status = target($this->_model)->add([
            'order_goods_id' => $info['id'],
            'user_id' => $userId,
            'money' => $money,
            'cause' => $cause,
            'content' => $content,
            'images' => serialize($images),
            'status' => 1,
            'create_time' => time(),
            'return_no' => target('order/Order','service')->logNo()
        ]);
        if (!$status) {
            target($this->_model)->rollBack();
            $this->stop('申请提交失败,请稍后再试!');
        }
        $status = target('order/OrderGoods')->edit([
            'id' => $info['id'],
            'service_status' => 1
        ]);
        if (!$status) {
            target($this->_model)->rollBack();
            $this->stop('申请提交失败,请稍后再试!');
        }
        target($this->_model)->commit();
        return $this->run('退货申请提交成功,等待工作人员审核!');
    }



    protected function cancel() {
        $returnNo = $this->params['return_no'];
        $userId = intval($this->params['user_id']);
        $info = target($this->_model)->getWhereInfo([
            'return_no' => $returnNo,
            'A.user_id' => $userId
        ]);
        if (empty($info)) {
            return $this->stop('该退货单不存在!');
        }
        if($info['status'] <> 1 && $info['status'] <> 2) {
            return $this->stop('该退货申请无法取消!');
        }
        target($this->_model)->beginTransaction();
        $status = target($this->_model)->edit([
            'return_id' => $info['return_id'],
            'status' => 0,
            'process_time' => time()
        ]);
        if (!$status) {
            target($this->_model)->rollBack();
            return $this->stop('申请取消失败,请稍后再试!');
        }

        $status = target('order/OrderGoods')->edit([
            'id' => $info['order_goods_id'],
            'service_status' => 0
        ]);
        if (!$status) {
            target($this->_model)->rollBack();
            return $this->stop('申请取消失败,请稍后再试!');
        }
        target($this->_model)->commit();
        return $this->run([], '退货申请取消成功!');
    }

}