<?php

/**
 * 购物车提交
 */

namespace app\order\middle;


class CartSubmitMiddle extends \app\base\middle\BaseMiddle {


    /**
     * 媒体信息
     */
    protected function meta() {
        $this->setMeta('结算购物车');
        $this->setName('结算购物车');
        $this->setCrumb([
            [
                'name' => '结算购物车',
                'url' => url()
            ]
        ]);

        return $this->run([
            'pageInfo' => $this->pageInfo
        ]);
    }

    protected function data() {
        $this->params['user_id'] = intval($this->params['user_id']);
        $addId = intval($this->params['add_id']);
        $addInfo = target('order/OrderAddress')->getAddress($this->params['user_id'], $addId);
        $urlParams['add_id'] = $addId;


        $addList = target('order/OrderAddress')->loadList(['user_id' => $this->params['user_id']]);
        $list = target('order/Cart', 'service')->getList($this->params['user_id']);
        $info = target('order/Cart', 'service')->getCart($this->params['user_id']);

        if (empty($list)) {
            return $this->stop('购物车商品为空');
        }
        $orderData = target('order/Order', 'service')->splitOrder($addInfo['province'], $list);

        $deliveryPrice = 0;
        foreach ($orderData as $vo) {
            $deliveryPrice += $vo['delivery_price'];
        }
        $orderPrice = $info['total'];



        $couponList = target('order/OrderCouponLog')->loadList([
            'A.user_id' => $this->params['user_id'],
            'A.status' => 0,
            'A.del' => 0,
            '_sql' => 'A.end_time >= ' . time()
        ]);

        foreach ($couponList as $key => $vo) {
            foreach ($orderData as $v) {
                if($vo['seller_id'] <> 0 || $vo['seller_id'] <> $v['seller_id']) {
                    continue 2;
                }
                if(!target($vo['typeInfo']['target'])->isCoupon($vo, $v)) {
                    unset($couponList[$key]);
                }
            }
        }

        return $this->run([
            'list' => $orderData,
            'info' => $info,
            'addList' => $addList,
            'deliveryPrice' => $deliveryPrice,
            'orderPrice' => $orderPrice,
            'addInfo' => $addInfo,
            'couponList' => $couponList
        ]);
    }

    protected function post() {
        $this->params['user_id'] = intval($this->params['user_id']);
        $codStatus = intval($this->params['cod_status']);
        $couponId = intval($this->params['coupon']);
        $target = target('order/Order', 'service');
        $orderNos = $target->addOrder($this->params['user_id'], '', $this->params['add_id'], $codStatus, $couponId ,$this->params['remark']);
        if (!$orderNos) {
            return $this->stop($target->getError());
        }
        target('order/Cart', 'service')->clear($this->params['user_id']);

        $orderList = target('order/Order')->loadList([
            '_sql' => 'order_no in(' . implode(',', $orderNos) . ')'
        ]);
        
        $deliveryPrice = 0;
        $orderPrice = 0;
        $app = [];
        foreach ($orderList as $vo) {
            $deliveryPrice += $vo['delivery_price'];
            $orderPrice += $vo['order_price'];
            $app[] = $vo['order_app'];
        }
        $app = array_unique($app);

        $accountInfo = target('member/PayAccount')->getWhereInfo([
            'A.user_id' => $this->params['user_id']
        ]);

        return $this->run(['cod_status' => $codStatus, 'order_no' => implode('|', $orderNos), 'order_price' => $orderPrice, 'delivery_price' => $deliveryPrice, 'user_money' => $accountInfo['money'], 'app' => $app]);
    }

}