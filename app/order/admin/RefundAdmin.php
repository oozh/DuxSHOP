<?php

/**
 * 退款管理
 * @author  Mr.L <349865361@qq.com>
 */

namespace app\order\admin;

class RefundAdmin extends \app\system\admin\SystemExtendAdmin {

    protected $_model = 'OrderRefund';

    /**
     * 模块信息
     */
    protected function _infoModule() {
        return [
            'info' => [
                'name' => '退款管理',
                'description' => '处理订单产品退货',
            ],
            'fun' => [
                'index' => true,
                'del' => true,
                'status' => true,
            ]
        ];
    }

    public function _indexParam() {
        return [
            'keyword' => 'B.refund_no',
            'type' => 'A.status',
        ];
    }

    public function _indexOrder() {
        return 'A.refund_id desc';
    }

    public function info() {
        $id = request('', 'id', 0);
        if (empty($id)) {
            $this->error('参数获取错误!');
        }
        $info = target($this->_model)->getInfo($id);
        if (!isPost()) {
            $orderInfo = target('order/Order')->getWhereInfo([
                'order_id' => $info['order_id']
            ]);
            if($info['admin_user_id']) {
                $adminInfo = target('system/SystemUser')->getInfo($info['admin_user_id']);
            }
            $this->assign('adminInfo', $adminInfo);
            $this->assign('info', $info);
            $this->assign('orderInfo', $orderInfo);
            $this->systemDisplay();
        } else {
            if(!$info['status']) {
                $this->error('该退款单无法处理!');
            }

            $status = request('', 'status' , 0, 'intval');
            $remark = request('', 'remark' , '', 'html_clear');

            if($info['status'] <> 1) {
                $this->error('当前状态无法操作!');
            }

            target($this->_model)->beginTransaction();
            $save = target($this->_model)->edit([
                'refund_id' => $id,
                'status' => $status ? 2 : 3,
                'admin_user_id' => $this->userInfo['user_id'],
                'admin_remark' => $remark,
                'process_time' => time()
            ]);
            if (!$save) {
                target($this->_model)->rollBack();
                $this->error('退款处理失败!');
            }

            if($status) {
                $save = target('order/OrderGoods')->edit([
                    'id' => $info['order_goods_id'],
                    'service_status' => 2
                ]);
                if (!$save) {
                    target($this->_model)->rollBack();
                    $this->error('退款处理失败!');
                }
                $status = target('member/Finance', 'service')->account([
                    'user_id' => $info['user_id'],
                    'money' => $info['money'],
                    'pay_no' => $info['refund_no'],
                    'pay_name' => '订单退款',
                    'type' => 1,
                    'remark' => '退款单号【'.$info['refund_no'].'】'
                ]);

                //被动接口
                $hookList = run('service', 'Order', 'hookRefundOrder', [
                    'data' => $info
                ]);
                if (!empty($hookList)) {
                    foreach ($hookList as $a => $vo) {
                        if (!$vo) {
                            $this->error(target($a . '/Order', 'service')->getError());
                        }
                    }
                }

                if(!$status) {
                    target($this->_model)->rollBack();
                    $this->error(target('member/Finance', 'service')->getError());
                }

                //关闭订单
                target('order/Order')->edit([
                    'order_id' => $info['order_id'],
                    'order_status' => 0
                ]);

            }else {
                $save = target('order/OrderGoods')->edit([
                    'id' => $info['order_goods_id'],
                    'service_status' => 0
                ]);
                if (!$save) {
                    target($this->_model)->rollBack();
                    $this->error('退款处理失败!');
                }
            }

            target($this->_model)->commit();

            $this->success('退款处理成功!', url('index'));


        }
    }


}