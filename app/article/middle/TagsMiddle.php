<?php

/**
 * Tag标签
 */

namespace app\article\middle;

class TagsMiddle extends \app\base\middle\BaseMiddle {

    protected $crumb = [];
    protected $tagInfo = [];
    protected $listWhere = [];
    protected $listOrder = [];
    protected $listLimit = 20;
    private $tpl = '';

    public function __construct() {
        parent::__construct();
        $this->tpl = $this->siteConfig['tpl_tags'];
    }


    private function getInfo() {
        if ($this->tagInfo) {
            return $this->tagInfo;
        }
        $name = $this->params['name'];
        $name = str_len(html_clear(urldecode($name)), 10, false);
        if (empty($name)) {
            return [];
        }
        $this->tagInfo = target('site/SiteTags')->getWhereInfo([
            'name' => $name
        ]);

        return $this->tagInfo;
    }

    protected function meta() {
        $info = $this->getInfo();
        if(empty($info)) {
            $this->stop('标签不存在', 404);
        }
        $this->setMeta($info['name']);
        $this->setName($info['name']);
        $this->setCrumb([
            [
                'name' => $info['name'],
                'url' => url('index', ['name' => $info['name']])
            ]
        ]);
        return $this->run([
            'pageInfo' => $this->pageInfo
        ]);
    }

    protected function data() {
        $info = $this->getInfo();
        if(empty($info)) {
            $this->stop('标签不存在', 404);
        }
        $where = [];
        $where['_sql'] = 'FIND_IN_SET("' . $info['tag_id'] . '", A.tags_id)';
        $order = 'A.create_time desc, B.article_id desc';

        $model = target('article/Article');
        $pageLimit = 15;
        $count = $model->countList($where);
        $pageData = $this->pageData($count, $pageLimit, $pageParams);
        $list = $model->loadList($where, $pageData['limit'], $order);





    }


}