<?php

/**
 * 订单管理
 */

namespace app\mall\middle;

class OrderMiddle extends \app\base\middle\BaseMiddle {

    protected function meta() {
        $this->setMeta('订单详情');
        $this->setName('订单详情');
        $this->setCrumb([
            [
                'name' => '我的订单',
                'url' => url('order/Order/index')
            ],
            [
                'name' => '订单详情',
                'url' => url()
            ]
        ]);

        return $this->run([
            'pageInfo' => $this->pageInfo
        ]);
    }


    protected function info() {
        $userId = intval($this->params['user_id']);
        $orderNo = html_clear($this->params['order_no']);
        if (empty($orderNo)) {
            return $this->stop('订单号不存在!', 404);
        }
        $orderInfo = target('mall/MallOrder')->getWhereInfo([
            'B.order_no' => $orderNo,
            'B.order_user_id' => $userId
        ]);
        if (empty($orderInfo)) {
            return $this->stop('订单不存在!', 404);
        }
        $payInfo = [];
        if ($orderInfo['pay_status']) {
            $payInfo = target('member/PayLog')->getInfo($orderInfo['pay_id']);
        }
        $deliveryInfo = [];
        if ($orderInfo['delivery_status']) {
            $deliveryInfo = target('order/OrderDelivery')->getWhereInfo([
                'A.order_id' => $orderInfo['order_id']
            ]);
        }
        $step = 1;
        if ($orderInfo['pay_type']) {
            if ($orderInfo['status_data']['action'] == 'pay') {
                $step = 1;
            }
            if ($orderInfo['status_data']['action'] == 'delivery' || $orderInfo['status_data']['action'] == 'parcel') {
                $step = 2;
            }
            if ($orderInfo['status_data']['action'] == 'receive') {
                $step = 3;
            }
            if ($orderInfo['status_data']['action'] == 'complete' || $orderInfo['status_data']['action'] == 'comment') {
                $step = 4;
            }
        } else {
            if ($orderInfo['status_data']['action'] == 'delivery' || $orderInfo['status_data']['action'] == 'parcel') {
                $step = 1;
            }
            if ($orderInfo['status_data']['action'] == 'receive') {
                $step = 2;
            }
            if ($orderInfo['status_data']['action'] == 'pay') {
                $step = 3;
            }
            if ($orderInfo['status_data']['action'] == 'complete' || $orderInfo['status_data']['action'] == 'comment') {
                $step = 4;
            }
        }
        $orderGoods = target('order/OrderGoods')->loadList([
            'order_id' => $orderInfo['order_id']
        ]);

        $logList = target('order/OrderLog')->loadList(['order_id' => $orderInfo['order_id'], 'type' => 1]);

        return $this->run([
            'payInfo' => $payInfo,
            'deliveryInfo' => $deliveryInfo,
            'info' => $orderInfo,
            'orderGoods' => $orderGoods,
            'orderStep' => $step,
            'logList' => $logList
        ]);
    }


}