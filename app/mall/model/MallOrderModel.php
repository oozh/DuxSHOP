<?php

/**
 * 订单管理
 */
namespace app\mall\model;

use app\system\model\SystemModel;

class MallOrderModel extends SystemModel {

    protected $infoModel = [
        'pri' => 'id',
    ];

    protected function base($where) {
        $base = $this->table('mall_order(A)')
            ->join('order(B)', ['B.order_id', 'A.order_id'])
            ->join('member_user(C)', ['C.user_id', 'B.order_user_id']);
        $field = ['A.*', 'B.*', 'C.email(user_email)', 'C.tel(user_tel)', 'C.nickname(user_nickname)' ];
        return $base
            ->field($field)
            ->where((array)$where);
    }

    public function loadList($where = array(), $limit = 0, $order = 'A.id desc') {
        $list = $this->base($where)
            ->limit($limit)
            ->order($order)
            ->select();
        if(empty($list)){
            return [];
        }
        foreach($list as $key => $vo) {
            $list[$key]['show_name'] = target('member/MemberUser')->getNickname($vo['user_nickname'], $vo['user_tel'], $vo['user_email']);
            $list[$key]['status_data'] = target('order/Order', 'service')->getAction($vo);
            $list[$key]['status_data']['html'] = target('order/Order', 'service')->orderActionHtml($list[$key]);

            $orderCurrency = unserialize($vo['order_currency']);
            $list[$key]['order_currency'] = $orderCurrency;
            $list[$key]['total_price'] = price_format($vo['delivery_price'] + $list[$key]['order_price']);

        }

        return $list;
    }

    public function countList($where = array()) {
        return $this->base($where)->count();
    }

    public function getWhereInfo($where) {
        $info = $this->base($where)->find();
        if(empty($info)) {
            return [];
        }
        $info['show_name'] = target('member/MemberUser')->getNickname($info['user_nickname'], $info['user_tel'], $info['user_email']);
        $info['status_data'] = target('order/Order', 'service')->getAction($info);
        $info['status_data']['html'] = target('order/Order', 'service')->orderActionHtml($info);
        $info['total_price'] = price_format($info['delivery_price'] + $info['order_price']);
        $info['order_currency'] = unserialize($info['order_currency']);
        return $info;
    }

    public function getInfo($id) {
        $where = [];
        $where['A.id'] = $id;
        return $this->getWhereInfo($where);
    }


}