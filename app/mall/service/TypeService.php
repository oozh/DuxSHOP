<?php

namespace app\mall\service;
/**
 * 类型接口
 */
class TypeService {

    /**
     * 搜索接口
     */
    public function getSearchType() {
        return [
            'mall' => [
                'name' => '商城',
                'url' => 'mall/Search/index'
            ],
        ];
    }

    /**
     * 优惠券接口
     */
    public function getCouponType() {
        return [
            'mall' => [
                'name' => '商品',
                'target' => 'mall/Mall'
            ],
        ];
    }
}

