<?php

/**
 * 订单管理
 * @author  Mr.L <349865361@qq.com>
 */

namespace app\mall\admin;

class OrderAdmin extends \app\system\admin\SystemExtendAdmin {

    protected $_model = 'MallOrder';

    /**
     * 模块信息
     */
    protected function _infoModule() {
        return [
            'info' => [
                'name' => '商城订单',
                'description' => '管理商城商品订单',
            ],
            'fun' => [
                'index' => true,
                'add' => true,
                'edit' => true,
                'del' => true,
                'status' => true
            ]
        ];
    }

    public function _indexParam() {
        return [
            'keyword' => 'B.order_no',
            'type' => 'type',
        ];
    }

    public function _indexWhere($whereMaps) {

        switch($whereMaps['type']) {
            case 1:
                $where = 'B.order_status = 1 AND B.pay_status = 0';
                break;
            case 2:
                $where = 'B.order_status = 1 AND B.parcel_status = 0';
                break;
            case 3:
                $where = 'B.order_status = 1 AND B.delivery_status = 0';
                break;
            case 4:
                $where = 'B.order_status = 1 AND B.delivery_status = 1 AND B.order_complete_status = 0';
                break;
            case 5:
                $where = 'B.order_status = 1 AND B.order_complete_status = 1';
                break;
            case 6:
                $where = 'B.order_status = 0';
                break;
        }
        if(!empty($where)) {
            $whereMaps['_sql'] = $where;
        }
        unset($whereMaps['type']);


        return $whereMaps;
    }

    public function _indexAssign($pageMaps) {
        $orderNo = $pageMaps['order_no'];
        return array(
            'order_no' => $orderNo
        );
    }

    public function _indexOrder() {
        return 'B.order_id desc';
    }


    public function info() {
        $id = request('get', 'id', 0, 'intval');
        if(empty($id)) {
            $this->error404();
        }
        $info = target('mall/MallOrder')->getInfo($id);
        if(empty($info)) {
            $this->error404();
        }
        $payInfo = [];
        if($info['pay_status']) {
            $payList = target('member/PayConfig')->typeList();
            $info['pay_way'] = $info['pay_way'] ? $info['pay_way'] : 'system';
            $payTypeInfo = $payList[$info['pay_way']];
            $payInfo = target($payTypeInfo['target'], 'pay')->getLog($info['pay_id']);
        }

        $deliveryList = target('order/OrderDelivery')->loadList([
            'A.order_id' => $info['order_id']
        ]);

        $logList = target('order/OrderLog')->loadList([
            'order_id' => $info['order_id']
        ]);

        $orderGoods = target('order/OrderGoods')->loadList([
            'order_id' => $info['order_id']
        ]);

        $status = target('order/Order', 'service')->getStatus($info);

        $this->assign('info', $info);
        $this->assign('payInfo', $payInfo);
        $this->assign('status', $status);
        $this->assign('deliveryList', $deliveryList);
        $this->assign('logList', $logList);
        $this->assign('orderGoods', $orderGoods);
        $this->systemDisplay();
    }

}